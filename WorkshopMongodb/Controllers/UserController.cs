﻿using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkshopMongodb.Models;

namespace WorkshopMongodb.Controllers
{
    public class UserController : Controller
    {
        string connectionString = "mongodb://10.252.240.54";

        IMongoClient client;
        IMongoDatabase database;
        IMongoCollection<UserModels> collection;

        public UserController()
        {
            client = new MongoClient(connectionString);
            database = client.GetDatabase("WorkshopMongodb");
            collection = database.GetCollection<UserModels>("users");
        }

        public ActionResult Index()
        {
            var filter = Builders<UserModels>.Filter.Empty;
            var documents = System.Threading.Tasks.Task.Run(() => collection.Find(filter).ToListAsync()).Result;
            return View(documents);
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(UserModels entity)
        {
            try
            {
                // TODO: Add insert logic here

                collection.InsertOne(entity);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(UserModels entity)
        {
            try
            {
                // TODO: Add update logic here
                var filter = Builders<UserModels>.Filter.Where(u => u.Id == entity.Id);
                var result = collection.ReplaceOne(filter, entity);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add update logic here
                var filter = Builders<UserModels>.Filter.Eq(u => u.Id, id);
                var result = collection.DeleteOne(filter);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
